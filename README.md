# Browserify boilerplate

Fast browserify setup

## Set up
```sh
$ git clone git@gitlab.com:wyllisMonteiro/browserify-ci.git
$ cd browserify-ci
$ npm i
$ mkdir dist
```

## Build
```sh
$ npm run build
```

## Watch
```sh
$ npm run watch
```

## See example
```sh
$ npm run build
```

Open `index.html` file and you should see in console `Hello world`

## What the pipeline does ?
- **build_job** : Builds app and creates 2 artifacts (`dist` directory and `variables.env` file which allows to set asset path in `release_job`)
- **prepare_tag_job** : creates 1 artifact (`variables.env` file which allows to set infos in `release_job` like version and description)
- **release_job** : Push new TAG and Release

## How to generate new release ?
- Update `VERSION` file
- Create new changelog file `<>version<>.md` (ex: 1.0.0.md), in `changelog/`
- Add changes description like
```md
- Set up CI
- Fix CI
```
- Commit and merge branch into `main` or `master` and that's it
